# xApi Comparison

## General

### Actor
```json
"actor": {
    "name": "Max Mustermann",
    "account": { 
        "type": "moodle",
        "identifier": "max.mustermann@email.de"
    }
}
```
|    Parameter     | Moodle  |  Onyx   | Blackboard |
| :--------------: | :-----: | :-----: | :--------: |
|       name       | &#9745; | &#9745; |     ?      |
|   account.name   | &#9745; |    ?    |     ?      |
| account.homepage | &#9745; | &#9745; |  &#9745;   |

### Verb

```json
"verb": {
  "id": "URI",
  "display": { 
    "en-US": "experienced"
  }
}
```
|   Parameter   | Moodle  |  Onyx   | Blackboard |
| :-----------: | :-----: | :-----: | :--------: |
|      id       | &#9745; | &#9745; |  &#9745;   |
| display.en-US | &#9745; | &#9745; |  &#9745;   |

## Assessment
### Object
```json
"object": {
    "definition": {
        "interactionType": "other",
        "name": {
            "en-US": "Title"
        },
        "description": {
            "en-US": "Description"
        },
        "type": "http://adlnet.gov/expapi/activities/cmi.interaction"
    },
    "id": "http://adlnet.gov/expapi/activities/moodle/{id}",
    "objectType": "Activity"
}
```
|          Parameter           | Moodle  |  Onyx   | Blackboard |
| :--------------------------: | :-----: | :-----: | :--------: |
|              id              |    ?    | &#9745; |     ?      |
|          objectType          | &#9745; | &#9745; |  &#9745;   |
|  definition.interactionType  | &#9745; | &#9745; |  &#9745;   |
|    definition.name.en-US     | &#9745; | &#9745; |     ?      |
| definition.description.en-US | &#9745; | &#9745; |     ?      |
|       definition.type        | &#9745; | &#9745; |  &#9745;   |

### Result
```json
"result": {
    "duration": "PT10S",
    "score": {
        "min": 0,
        "scaled": 1,
        "max": 10,
        "raw": 10
    },
    "success" : true
}
```

|  Parameter   | Moodle |  Onyx   | Blackboard |
| :----------: | :----: | :-----: | :--------: |
|   duration   |   ?    | &#9745; |     ?      |
|  score.min   |   ?    | &#9745; |     ?      |
| score.scaled |   ?    | &#9745; |     ?      |
|  score.max   |   ?    | &#9745; |     ?      |
|  score.raw   |   ?    | &#9745; |     ?      |
|   success    |   ?    | &#9745; |     ?      |
